<?php
	require_once("conexao.php");
	require_once("../Modelo/TextModel.php");
	class TextControl{

		function SelecTudoTextTables($table){
			try{
				$con = new conexao();
				$cmd = $con->getConexao()->prepare("SELECT conteudo FROM $table;");
				$cmd->execute();
				$resultado = $cmd->fetchAll(PDO::FETCH_CLASS,"TextModel");
				return $resultado;
			}catch(PDOException $e){
		 		echo"<p>Erro no banco: {$e->getMessage()}.</p>";
	 		}catch(Exception $e){
	 			echo"<p>Erro geral no sistema: {$e->getMessage()}.</p>";
	 		}
		}

		function updateTextTables($nomeTable,$post){
				try{
					$con = new conexao();
					$cmd = $con->getConexao()->prepare("UPDATE $nomeTable set conteudo = :ct WHERE id=1;");
					$cmd->bindParam("ct",$post->getConteudo());
					if($cmd->execute()){
						return true;
					}else{
						return false;
						$con->fecharConexao();
					}
				}catch(PDOException $e){
					echo"<p>Erro no banco: {$e->getMessage()}.</p>";
				}catch(Exception $e){
					echo"<p>Erro geral no sistema: {$e->getMessage()}.</p>";
				}
		}

		function insertText($nomeTable,$post){
			try{
				$con = new conexao();
				$cmd = $con->getConexao()->prepare("INSERT INTO $nomeTable(conteudo)VALUES(:cd);");
				$cmd->bindParam("cd",$post);
				if($cmd->execute()){
					return true;
				}else{
					return false;
					$con->fecharConexao();
				}
			}catch(PDOException $e){
				echo"<p>Erro no banco :{$e->getMessage()}</p>";
			}catch(Exception $e){
				echo"<p>Erro geral no sistema: {$e->getMessage()}</p>";
			}
		}
		function deleteText($nomeTable){
			try{
				$con = new conexao();
				$cmd = $con->getConexao()->prepare("TRUNCATE $nomeTable;");
				if($cmd->execute()){
					return true;
				}else{
					return false;
					$con->fecharConexao();
				}
			}catch(PDOException $e){
				echo"<p>Erro no banco : {$e->getMessage()}</p>";
			}catch(Exception $e){
				echo"<p>Erro geral no sistema :{$e->getMessage()}</p>";
			}
		}
	}
?>