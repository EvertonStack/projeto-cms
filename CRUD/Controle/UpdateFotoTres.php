<?php
	require_once("LogicaImagem.php");
	require_once("../Modelo/ImageModel.php");
	try{
		$control = new LogicaImagem();
		$model =  new ImageModel();
		$id = $_GET['id'];
		$file = $_FILES['foto-tres'];
		$model->setNome($file['name']);
		$model->setTipo($file['type']);
		$model->setBinario(file_get_contents($file['tmp_name']));
		if($control->updateImagem("foto_tres",$model,$id)){
			header("Location: ../Visual/painel.php");
		}
	}catch(Exception $e){
		echo"<p>Erro: {$e->getMessage()}</p>";
	}
?>