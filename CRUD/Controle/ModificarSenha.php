<?php 
	require_once("../Modelo/senhaModel.php");
	require_once("UserControl.php");
try{
	$model = new senhaModel();
	$control = new UserControl();
	$model->setSenha($control->criptografarSenha($_POST['password']));
	if($control->modificarSenha($model) && $_POST['nomeUsuario'] == 'admin'){
		header('Location: ../Visual/login.php');
	}else{
		header("Location: ../Visual/esqueciSenha.php");
	}
}catch(PDOException $e){
	echo"<p>Erro no banco: {$e->getMessage()}</p>";
}catch(Exception $e){
	echo"<p>Erro geral no sistema: {$e->getMessage()}</p>";
}