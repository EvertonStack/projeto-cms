<?php
	require_once("conexao.php");
	require_once("../Modelo/UserModel.php");
	require_once("../Modelo/senhaModel.php");
	class UserControl{
		function criptografarSenha($txt){
			$file = fopen("KeyLog.txt","r");
			$key = fread($file,4096);
			$cifra = "AES-128-CBC";
			$size = openssl_cipher_iv_length($cifra);
			$vc = openssl_random_pseudo_bytes($size);
			$criptografia = openssl_encrypt($txt,$cifra,$key,OPENSSL_RAW_DATA,$vc);
			$criptografia = base64_encode($vc.$criptografia);
			return $criptografia;
		}

		function descriptografarSenha($txt){
			$txtCrip = base64_decode($txt);
			$file = fopen("KeyLog.txt","r");
			$key = fread($file,4096);
			$cifra = "AES-128-CBC";
			$size = openssl_cipher_iv_length($cifra);
			$vc = mb_substr($txtCrip,0,$size,'8bit');
			$txtCrip = mb_substr($txtCrip,$size,null,'8bit');
			$descript = openssl_decrypt($txtCrip,$cifra,$key,OPENSSL_RAW_DATA,$vc);
			return $descript;
		}
		 function modificarSenha($model){
			try{
				$con = new conexao();
				$cmd  = $con->getConexao()->prepare("UPDATE senha set senha = :se where id = 1;");
				$cmd->bindParam(":se",$model->getSenha());
				if($cmd->execute()){
					return true;
					header("Location: ../Visual/login.php");
				}else{
					$con->fecharConexao();
					return false;
				}
			}catch(PDOException $e){
				$con->fecharConexao();
				echo"<p>Erro no banco: {$e->getMessage()}.</p>";
			}catch(Exception $e){
				$con->fecharConexao();
				echo"<p>Erro geral no sistema: {$e->getMessage()}.</p>";
			}
		}
		function SelecionarTudoUser(){
			try{
				$con = new conexao();
				$cmd = $con->getConexao()->prepare("SELECT * FROM admin;");
				$cmd->execute();
				$resultado = $cmd->fetchAll(PDO::FETCH_CLASS,"UserModel");
				return $resultado;
			}catch(PDOException $e){
		 		echo"<p>Erro no banco: {$e->getMessage()}.</p>";
	 		}catch(Exception $e){
	 			echo"<p>Erro geral no sistema: {$e->getMessage()}.</p>";
	 		}
		}
		function SelecionarTudoSenha(){
			try{
				$con = new conexao();
				$cmd = $con->getConexao()->prepare("SELECT senha FROM senha;");
				$cmd->execute();
				$resultado = $cmd->fetchAll(PDO::FETCH_CLASS,"SenhaModel");
				return $resultado;;
			}catch(PDOException $e){
		 		echo"<p>Erro no banco: {$e->getMessage()}.</p>";
	 		}catch(Exception $e){
	 			echo"<p>Erro geral no sistema: {$e->getMessage()}.</p>";
	 		}
		}
	}
?>