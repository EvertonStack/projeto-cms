<?php
	class conexao{
		private $conexao;
		function __construct(){
			$dbname = "ImaginAll";
			$user = "root";
			$host = "localhost";
			$password = "password";
			try{
				$this->setConexao(new PDO("mysql:host={$host};dbname={$dbname}",$user,$password));
				$this->getConexao()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}catch(PDOException $e){
				echo "<p>Erro no banco: {$e->getMessage()}</p>";
			}catch(Exception $e){
				echo "<p>Erro geral no sistema: {$e->getMessage()}</p>";
			}
		}

		public function getConexao(){
			return $this->conexao;
		}
		public function setConexao($data){
			$this->conexao = $data;
		}
		public function fecharConexao(){
			$this->conexao = NULL;
		}
	}
?>