<?php
	ini_set('mysql.connect_timeout',300);
	ini_set('default_socket_timeout',300);
	require_once("conexao.php");
	require_once("../Modelo/ImageModel.php");
	class LogicaImagem{
		function inserirImagem($nomeTable,$img){	
			try{
				$con = new conexao();
				$cmd = $con->getConexao()->prepare("INSERT INTO $nomeTable(nome,tipo,binario)VALUES(:n,:t,:d);");
				$cmd->bindParam("n",$img['name']);
				$cmd->bindParam("t",$img['type']);
				$bin = file_get_contents($img['tmp_name']);
				$cmd->bindParam("d",$bin);  
				if($cmd->execute()){
					return true;
					$con->fecharConexao();
				}else{
					return false;
					$con->fecharConexao();
				}
			}catch(PDOException $e){
		 		echo"<p>Erro no banco: {$e->getMessage()}.</p>";
	 		}catch(Exception $e){
	 			echo"<p>Erro geral no sistema: {$e->getMessage()}.</p>";
	 		}
		}
		function updateImagem($nameTable,$img,$id){
			try{
				$con = new conexao();
				$cmd = $con->getConexao()->prepare("UPDATE $nameTable SET nome=:nm,tipo=:tp,binario=:bn WHERE id=:id;");
				$cmd->bindParam("nm",$img->getNome());
				$cmd->bindParam("tp",$img->getTipo());
				$cmd->bindParam("bn",$img->getBinario());
				$cmd->bindParam("id",$id);
				if($cmd->execute()){
					return true;
				}else{
					return false;
					$con->fecharConexao();
				}
			}catch(PDOException $e){
		 		echo"<p>Erro no banco: {$e->getMessage()}.</p>";
	 		}catch(Exception $e){
	 			echo"<p>Erro geral no sistema: {$e->getMessage()}.</p>";
	 		}
		}
	function SelecionarImagem($nomeTable){
		try{
			$con = new conexao();
			$cmd = $con->getConexao()->prepare("SELECT nome FROM $nomeTable;");
			$cmd->execute();
			$resultado = $cmd->fetchAll(PDO::FETCH_CLASS,'ImageModel');
			return $resultado;
		}catch(PDOException $e){
		 		echo"<p>Erro no banco: {$e->getMessage()}.</p>";
 		}catch(Exception $e){
 			echo"<p>Erro geral no sistema: {$e->getMessage()}.</p>";
 		}
	} 
	function SelecionarUm($id,$nomeTable){
		try{
			$con = new conexao();
			$cmd = $con->getConexao()->prepare("SELECT * FROM $nomeTable WHERE id=:id;");
			$cmd->bindParam("id",$id);
			$cmd->execute();
			$resultado = $cmd->fetch(PDO::FETCH_OBJ);
			return $resultado;
		}catch(PDOException $e){
		 		echo"<p>Erro no banco: {$e->getMessage()}.</p>";
 		}catch(Exception $e){
 			echo"<p>Erro geral no sistema: {$e->getMessage()}.</p>";
 		}
	}
	function deleteImage($nomeTable){
			try{
				$con = new conexao();
				$cmd = $con->getConexao()->prepare("TRUNCATE $nomeTable;");
				if($cmd->execute()){
					return true;
				}else{
					return false;
					$con->fecharConexao();
				}
			}catch(PDOException $e){
				echo"<p>Erro no banco : {$e->getMessage()}</p>";
			}catch(Exception $e){
				echo"<p>Erro geral no sistema :{$e->getMessage()}</p>";
			}
		}
}
?>