<?php
	session_start();
	if(isset($_SESSION['nomeUser'])){
		header("Location: painel.php");
	}else{
		echo"
			<html>
				<head>
					<meta charset='UTF-8'>
					<title>Login</title>
					<link rel='stylesheet' type='text/css' href='CSS/reset.css'>
					<link rel='stylesheet' type='text/css' href='CSS/credenciais.css'>
					<link rel='icon' href='../../IMAGES/person-register.png'>
				</head>
				<body>
					<section class='container'>
						<article class='right-part'>
							<img src='../../IMAGES/novaIdeia.svg' alt='imagem lateral de nova ideia' class='img'>					
						</article>
						<div class='form'>
							<form action='../Controle/logPart.php' method='POST' id='form'>
								<div id='form-title'><p id='title'>Entrar</p></div>
								<div class='column-inputs'>
									<input type='text' name='nomeUser' class='inputs' id='nomeUser' placeholder='Nome de Usuário' required/></br>
									<input type='password' name='senha' class='inputs' id='senha' placeholder='Sua senha' required/></br>
									<input type='submit' value='Entrar' id='btn'></br>
								</div>
								<div class='linkagem'>
									<div class='organization'>
										<a href='../../index.php' class='links' id='link-menu'>Menu</a>
									</div>
								</div>
								<div id='link-new-senha'><p><a href='esqueciSenha.php' id='forget-password'>Esqueci a senha</a></p></div>
							</form>
						</div>
						</section>
				</body>
			</html>
		";
	}
?>