<?php
	require_once("../Controle/TextControl.php");
	require_once("../Controle/LogicaImagem.php");
	$control = new TextControl();
	$controlImage = new LogicaImagem();
	$modelNome = $control->SelecTudoTextTables('name_site');
	$modelDesc = $control->SelecTudoTextTables('description_user');
	$modelInitial = $control->SelecTudoTextTables('initial_text');
	$modelFooter = $control->SelecTudoTextTables('footer_text');
	$modelLeftText = $control->SelecTudoTextTables('left_text');
	$modelRightText = $control->SelecTudoTextTables('right_text');
	$modelIcone = $controlImage->SelecionarImagem("imgicone");
	$modelFotoUser = $controlImage->SelecionarImagem("foto_user");
	$modelFotoUm = $controlImage->SelecionarImagem("foto_um");
	$modelFotoDois = $controlImage->SelecionarImagem("foto_dois");
	$modelFotoTres = $controlImage->SelecionarImagem("foto_tres");
		echo"
		<head>
			<title>Meu blog</title>
			<meta charset='UTF-8'/>
			<link rel='styleshet' href='CSS/reset.css'>
			<link rel='stylesheet' href='CSS/ModificationSite.css'>
			<link rel='icon' href='../../IMAGES/browser.png'>
		</head>
		<body>
			<header id='header'>
			<a href='../../'><img src='../../IMAGES/backRow.png' id='row-back'/></a>
				<div class='header-components'>";
				if($modelIcone==NULL){
					echo"<img src='../../IMAGES/logo.svg' id='logo'/>";
				}else{
					echo"<img src='../Controle/carregaImagem.php?id=1' id='logo' name='foto-icone'/>";
				}
					 echo"<h1>";
						if($modelNome != NULL){
								foreach($modelNome as $check){
							 		echo"<h1>{$check->getConteudo()}</h1>";
							 	}
						}else{
								echo"<h1>Imaginall</h1>";
							}
					echo"</h1>
					</div>
					<a href='login.php' id='button-nav'><button type='button' class='button'>Modificar</button></a>
					</header>
					<section class='all-center'>
					<div class='first-visual'>
						<div class='card-organization'>
							
								<div class='center'>";
								if($modelFotoUser==NULL){
									echo"<img src='../../IMAGES/person-register.png' id='user-img'>";
								}else{
									echo"<img src='../Controle/carregaFotoDesc.php?id=1' id='user-img' name='foto-perfil'>";
								}
								echo"</div>
								<div class='text-organization'>
									<div id='hehe'>
										<div id='center-text'>
										<p>";
											  if($modelDesc!=NULL){
											 	foreach($modelDesc as $desc){
											 		echo"<p id='text-desc'>{$desc->getConteudo()}</p>";
											 	}
											 }else{
											 	echo"<p id='text-desc'>Texto do Perfil</p>";
											 }
										echo"</p>
										</div>
									</div>
								</div>";
						
						echo"</div>
						<article class='text'>
							<p class='text-organization' class='reform'>";	
							 if($modelInitial!=NULL){
							 	foreach($modelInitial as $md){
							 		echo"<p class='text-organization' id='text-reform'>{$md->getConteudo()}</p>";
							 	}
							 }else{
							 	echo"<p class='text-organization' id='text-reform'>Texto inicial</p>";
							 }
							echo"</p>
						</article>
					</div>
					<div id='fotos'>
						<div class='imgs'>";
							if($modelFotoUm==NULL){
								echo"<img src='../../IMAGES/back-gallery.jpg' class='img'>";
							}else{
								echo"<img src='../Controle/carregaFotoUm.php?id=1' class='img'>";
							}if($modelFotoDois==NULL){
							 echo"<img src='../../IMAGES/back.jpg' class='img'>";
							}else{
								echo"<img src='../Controle/carregaFotoDois.php?id=1' class='img'>";
							}if($modelFotoTres==NULL){
								echo"<img src='../../IMAGES/backPaint.jpeg' class='img'>";
							}else{
							 	echo"<img src='../Controle/carregaFotoTres.php?id=1' class='img'>";
							}
						echo"</div>
						<div class='text-bottom'>
							<article class='text-align'>
								<p class='text-organization' id='text-left'>";
									if($modelLeftText!=NULL){
										foreach($modelLeftText as $leftText){
											echo"<p class='bottom-text'>{$leftText->getConteudo()}</p>";
										}
									}else{
										echo"<p class='bottom-text'>Texto da esquerda</p>";
									}
								echo"</p>
							</article>
							<article id='text-align-right' class='bottom-text'>";
									if($modelRightText!=NULL){
										foreach($modelRightText as $rightText){
											echo"<p class='bottom-text' id='um'>{$rightText->getConteudo()}</p>";
										}
									}else{
										echo"<p class='bottom-text' id='um'>Texto da direita</p>";
									}
							echo"</article>
					</div>
					</div>
				</section>
				<footer>
					<div id='slogan'>";
						if($modelFooter!=NULL){
							foreach($modelFooter as $modelCont){
								echo"<p id='word'>{$modelCont->getConteudo()}</p>";
							}
						}else{
							echo"<p id='word'>Texto rodapé</p>";
						}
					echo"</div>
				</footer>
		</body>";
?>