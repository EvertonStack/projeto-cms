<?php
	ini_set('mysql.connect_timeout',300);
	ini_set('default_socket_timeout',300);
	require_once("../Controle/TextControl.php");
	require_once("../Controle/LogicaImagem.php");
	$controlText = new TextControl();
	$controlImage = new LogicaImagem();
	$modelIcone = $controlImage->SelecionarImagem('imgicone');
	$modelFotoUser = $controlImage->SelecionarImagem('foto_user');
	$modelFotoUm = $controlImage->SelecionarImagem('foto_um');
	$modelFotoDois = $controlImage->SelecionarImagem('foto_dois');
	$modelFotoTres = $controlImage->SelecionarImagem('foto_tres');
	$modelNome = $controlText->SelecTudoTextTables('name_site');
	$modelDesc = $controlText->SelecTudoTextTables('description_user');
	$modelInitial = $controlText->SelecTudoTextTables('initial_text');
	$modelFooter = $controlText->SelecTudoTextTables('footer_text');
	$modelLeftText = $controlText->SelecTudoTextTables('left_text');
	$modelRightText = $controlText->SelecTudoTextTables('right_text');
	session_start();
	if(isset($_SESSION['nomeUser'])){
	echo "<html>
		<head>
			<meta charet='UTF-8'/>
			<link rel='stylesheet' href='formTeste.css'/>
			<link rel='icon' href='../../IMAGES/engrenagem.png'>
			<link rel='stylesheet' href='CSS/painel.css'>
			<title>Painel de controle</title>
		</head>
		<body>";
		if($modelIcone==NULL){
				echo"<form action='../Controle/insertIcone.php' method='POST' enctype='multipart/form-data' class='form-one'>
					<section><label>Foto Ícone:</label><input type='file' name='foto-icone' /><div class='inserirDiv'><input type='submit' value='Inserir' class='btn-inserir'></div></section>
				</form>";
			}else{
				echo"
				<form action='../Controle/UpdateIcone.php?id=1' method='POST' enctype='multipart/form-data' class='form-one'><section><label>Foto Icone:</label><input type='file' name='foto-icone'><div class='div'><input type='submit' value='Atualizar'/></form><form action='../Controle/deleteFotoIcone.php' method='POST' id='one-form'><input type='submit' value='Deletar' class='button-delete'></div></section></form>
				</form>";
			}
			if($modelNome==NULL){
				echo"<form action='../Controle/insertNomeSite.php' method='POST'>
				<section><label>Nome site:</label><input type='text' name='nome-site'/><div class='inserirDiv'><input type='submit' value='Inserir' class='btn-inserir'></div></section></form>";
			}else{
					foreach($modelNome as $check){
						echo"<form action='../Controle/UpdateNomeSite.php' method='POST'>
						<section><label>Nome site:</label><input type='text' name='nome-site' value='{$check->getConteudo()}'/><div class='div'><input type='submit' value='Atualizar'/></form><form action='../Controle/deleteNomeSite.php' method='POST' class='second-form' id='two'><input type='submit' value='Deletar' class='button-delete'/></form></div></section>";		 		
				 	}
			}
			if($modelFotoUser==NULL){
			echo"<form action='../Controle/insertFotoPerfil.php' method='POST' enctype='multipart/form-data'>
					<section><label>Foto perfil:</label><input type='file' name='foto-perfil'/><div class='inserirDiv'><input type='submit' value='Inserir' class='btn-inserir'></div></section>
				</form>";
			}else{
				echo"<form action='../Controle/UpdateFotoDesc.php?id=1' method='POST' enctype='multipart/form-data'>
					<section><label>Foto perfil:</label><input type='file' name='foto-perfil'/><div class='div'><input type='submit' value='Atualizar'></form><form action='../Controle/deleteFotoUser.php' method='POST' class='second-form' id='three'><input type='submit' value='Deletar' class='button-delete'/></form></div></section>";
			}
			if($modelDesc==NULL){
				echo"<form action='../Controle/insertTextDesc.php' method='POST'>
						<section><label>Texto perfil:</label><textarea name='text_desc'></textarea><div class='inserirDiv'><input type='submit' value='Inserir' class='btn-inserir'></div></section>
				</form>";
			}else{
				foreach($modelDesc as $desc){
					echo"<form action='../Controle/UpdateDesc.php' method='POST'>
						<section><label>Texto perfil:</label><textarea name='text_desc'>{$desc->getConteudo()}</textarea><div class='div'><input type='submit' value='Atualizar'/></form><form action='../Controle/deleteDescText.php' method='POST' class='second-form' id='four'><input type='submit' value='Deletar' class='button-delete'></form></div></section>";
					}
			}
			if($modelInitial==NULL){
				echo"<form action='../Controle/insertTextInicial.php' method='POST'>
						<section><label>Texto inicial:</label><textarea name='initial-text'></textarea><div class='inserirDiv'><input type='submit' value='Inserir' class='btn-inserir'></div></section>
					</form>";
			}else{
				foreach($modelInitial as $initial){
				echo"<form action='../Controle/UpdateInitialText.php' method='POST'>
						<section><label>Texto inicial:</label><textarea name='initial-text'>{$initial->getConteudo()}</textarea><div class='div'><input type='submit' value='Atualizar'></form><form action='../Controle/deleteInitialText.php' method='POST' class='second-form' id='five'><input type='submit' value='Deletar' class='button-delete'></form></div></section>";
					}
			}
			if($modelFotoUm==NULL){
				echo"<form action='../Controle/insertFotoUm.php' method='POST' enctype='multipart/form-data'>
					<section><label>Foto 1:</label><input type='file' name='foto-um' /><div class='inserirDiv'><input type='submit' value='Inserir' class='btn-inserir'></div></section>
				</form>";
			}else{
				echo"<form action='../Controle/UpdateFotoUm.php?id=1' method='POST' enctype='multipart/form-data'>
					<section><label>Foto 1:</label><input type='file' name='foto-um'/><div class='div'><input type='submit' value='Atualizar'></form><form action='../Controle/deleteFotoUm.php' method='POST' class='second-form' id='six'><input type='submit' value='Deletar' class='button-delete'/></form></div></section>";
			}
			if($modelFotoDois==NULL){
				echo"<form action='../Controle/insertFotoDois.php' method='POST' enctype='multipart/form-data'>
					<section><label>Foto 2:</label><input type='file' name='foto-dois'><div class='inserirDiv'><input type='submit' value='Inserir' class='btn-inserir'></div></section></form>";
			}else{
				echo"<form action='../Controle/UpdateFotoDois.php?id=1' method='POST' enctype='multipart/form-data'>
					<section><label>Foto 2:</label><input type='file' name='foto-dois'/><div class='div'><input type='submit' value='Atualizar'/></form><form action='../Controle/deleteFotoDois.php' method='POST' class='second-form' id='seven'><input type='submit' value='Delete' class='button-delete'></form></div></section>";
			}
			if($modelFotoTres==NULL){
					echo"<form action='../Controle/insertFotoTres.php' method='POST' enctype='multipart/form-data'>
					<section><label>Foto 3:</label><input type='file' name='foto-tres'/><div class='inserirDiv'><input type='submit' value='Inserir' class='btn-inserir'></div></section></form>";	
			}else{
				echo"<form action='../Controle/UpdateFotoTres.php?id=1' method='POST' enctype='multipart/form-data'>
					<section><label>Foto 3:</label><input type='file' name='foto-tres'/><div class='div'><input type='submit' value='Atualizar'/></form><form action='../Controle/deleteFotoTres.php' method='POST' class='second-form' id='eight'><input type='submit' value='Deletar' class='button-delete'></form></div></section>";
			}
			if($modelLeftText==NULL){
			echo"<form action='../Controle/insertLeftText.php' method='POST'>
				<section><label>Esquerdo:</label><textarea name='left-text'></textarea><div class='inserirDiv'><input type='submit' value='Inserir' class='btn-inserir'></div></section></form>";
			}else{
				foreach($modelLeftText as $leftText){
				echo"<form action='../Controle/UpdateLeftText.php' method='POST'>
				<section><label>Esquerdo:</label><textarea name='left-text'>{$leftText->getConteudo()}</textarea><div class='div'><input type='submit' value='Atualizar'></form><form action='../Controle/deleteLeftText.php' method='POST' class='second-form' id='nine'><input type='submit' value='Deletar' class='button-delete'></form></div></section>";
				}
			}
			if($modelRightText==NULL){
			echo"<form action='../Controle/insertRightText.php' method='POST'>
				<section><label>Direito:</label><textarea name='right-text'></textarea><div class='inserirDiv'><input type='submit' value='Inserir' class='btn-inserir'></div></section></form>";
			}else{
				foreach($modelRightText as $rightText){
				echo"<form action='../Controle/UpdateRightText.php' method='POST'>
				<section><label>Direito:</label><textarea name='right-text'>{$rightText->getConteudo()}</textarea><div class='div'><input type='submit' value='Atualizar'></form><form action='../Controle/deleteRightText.php' method='POST' class='second-form' id='ten'><input type='submit' value='Deletar'  class='button-delete'></form></div></section>";
				}
			}
			if($modelFooter==NULL){
			echo"<form action='../Controle/insertFooterText.php' method='POST'>
				<section><label>Texto rodapé:</label><input type='text' name='footer-text'/><div class='inserirDiv'><input type='submit' value='Inserir' class='btn-inserir'></div></section></form>";
			}else{
				foreach($modelFooter as $footer){
				echo"<form action='../Controle/UpdateFooterText.php' method='POST'>
					<section><label>Texto rodapé:</label><input type='text' name='footer-text' value='{$footer->getConteudo()}'/><div class='div'><input type='submit' value='Atualizar'></form><form action='../Controle/deleteFooterText.php' method='POST' class='second-form' id='eleven'><input type='submit' value='Deletar' class='button-delete'></form></div></section>";	
					}			
			}
			echo"<div id='btns'>
				<a href='ModificationSite.php'><button type='button' class='btn-back'>Voltar</button></a>
				<a href='Atual.php'><button type='button' class='btn-back'>Ver resultado</button></a></div>
		</body>
	</html>";
}else{
	header("Location: login.php");
}
?>