<?php
	echo"
		<html>
			<head>
				<title>Portfólio da ImaginAll</title>
				<meta name='viewport' content='width=device-width, initial-scale=1.0'>
				<meta charset='UTF-8'>
				<link rel='stylesheet' href='CSS/portfolio.css'>
				<link rel='icon' href='../../IMAGES/galeria-icon.png'>
			</head>
			<body>
				<div class='title'>Nossa arte <img src='../../IMAGES/brush.png' class='logo'></div>
				<section class='body-port'>
					<div><img src='../../IMAGES/ARTES/quadro6.jpg'></div>
					<div><img src='../../IMAGES/ARTES/quadro5.jpg'></div>
					<div><img src='../../IMAGES/ARTES/quadro4.jpg' class='quadro-reform'></div>
					<div><img src='../../IMAGES/ARTES/quadro7.jpg' class='quadro-reform'></div>
					<div><img src='../../IMAGES/ARTES/quadro8.jpg'></div>
					<div><img src='../../IMAGES/ARTES/quadro3.jpg'></div>
					<div><img src='../../IMAGES/ARTES/quadro9.jpg' class='quadro-reform'></div>
					<div><img src='../../IMAGES/ARTES/quadro1.jpg' class='quadro-reform'></div>
					<div><img src='../../IMAGES/ARTES/quadro2.jpg' class='quadro-reform'></div>
				</section>
				<a href='../../'><img src='../../IMAGES/backRow.png' id='backRow'/></a>
			</body>
		</html>
	";
?>