<?php
	echo"
		<head>
			<title>Aprenda Agora!</title>
			<meta charset='UTF-8'>
			<link rel='stylesheet' href='CSS/reset.css'>
			<link rel='stylesheet' href='CSS/comoUsar.css'>
			<link rel='icon' href='../../IMAGES/intelectual.png'>
		</head>
		<body>
			<section class='menu'>
				<aside class='container-organization'>
					<h1>Divirta-se</h1>
					<article class='citacao'>A imaginação é sua aliada</article>
					<img src='../../IMAGES/logo.svg' id='logo'>
					<a href='#txt'><img src='../../IMAGES/backRow.png' id='seta'></a>
					<a href='../../'><img src='../../IMAGES/backRow.png' id='setaDois'></a>
				</aside>
			</section>
			<section class='conteudo'>
				<article class='content' class='first-cont' id='txt'><p class='text'>Nesta página você irá encontrar uma simples explicação de como utilizar nosso site. Cada link ao lado irá levá-lo(a) para à explicação de sua preferência.</p>
					<aside class='links-cont'>
						<a href='#fotos' class='links'>Fotos</a>
						<a href='#painel' class='links'>Painel</a>
						<a href='#acesso' class='links'>Acesso</a>
					</aside>
				</article>
			</section>
			<section class='conteudo'>
				<h2 id='fotos'>FOTOS</h2>
				<article class='content'>
					<p class='text'>Você deve escolher uma foto por vez, e as mesmas</br> devem ser apenas arquivos do tipo foto. Exemplo:</br> .png, .jpg, .svg ou .mpeg</p>
					<aside class='links-cont'>
						<div><img src='../../IMAGES/gallery.svg' class='imagens'></div>
					</aside>
				</article>
			</section>
			<section class='conteudo' id='painel'>
				<h2 id='fotos'>PAINEL</h2>
				<article class='content'>
					<p class='text'>Local onde você irá modificar seu site, tendo as opções de inserir, atualizar e deletar fotos e textos.</p>
					<aside class='links-cont'>
						<div><img src='../../IMAGES/admin-confirm.svg' class='imagens' id='mao'></div>
					</aside>
				</article>
			</section>
			<section class='conteudo' id='acesso'>
				<h2 id='fotos'>ACESSO</h2>
				<article class='content'>
					<p class='text'>Apenas o administrador tem a possibilidade
					de acessar e modificar o 'Meu blog'</p>
					<aside class='links-cont'>
						<div><img src='../../IMAGES/admin-log.svg' class='imagens' id='admin-ajust'></div>
					</aside>
				</article>
			</section>
		</body>
	";
?>