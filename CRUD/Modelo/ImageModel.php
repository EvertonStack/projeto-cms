<?php
	class ImageModel{
		private $nome;
		private $tipo;
		private $binario;

		function setNome($nome){
			$this->nome = $nome;
		}
		function getNome(){
			return $this->nome;
		}
		function setTipo($tipo){
			$this->tipo = $tipo;
		}
		function getTipo(){
			return $this->tipo;
		}
		function setBinario($binario){
			$this->binario = $binario;
		}
		function getBinario(){
			return $this->binario;
		}
	}
?>