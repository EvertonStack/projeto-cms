<?php 
	echo "
		<html>
	<head>
		<title>ImaginALL</title>
		<meta chaset='utf-8'/>
		<meta name='viewport' content='width=device-width, initial-scale=1.0'>
		<link rel='stylesheet' href='CRUD/Visual/CSS/index.css'>
		<link rel='icon' href='IMAGES/brush.png'>
	</head>
    <body>
    	<section class='box' id='here'>
    		<aside class='main'>
	    		<div id='container'>    		   			  
	    			<article class='word'>ImaginALL</article>
	    		</div>
    		</aside>

    		<aside class='second-part'>
    			<div class='left-image'>
    				<img src='IMAGES/creative.svg' alt='imagem de uma pintora' class='svg-image'>
    			</div>
    			<div class='container-news-paper'>
	    			<h3 class='title'>Quem somos ?</h3>
		    			<div class='news-paper'><article> Temos o prazer de informar, que somos a maior galeria da América Latina.
		    			Temos como objetivo mostrar o que fazemos de melhor, ARTE.</article></div>
	    		</div>
    		</aside>
    		
    		<aside class='terceira-parte'>
    			<div class='left-image'>
    				<img src='IMAGES/model.svg' alt='homem de negócios' class='svg-image'>
    			</div>
    			<div class='container-news-paper'>
	    			<h3 class='title'>Nossas vitórias !</h3>
		    			<div class='news-paper'><article> Temos parcerias com Alura Cursos, <br/>Campus Virtual Ntec, Moldsoft,<br/> Arco Educação e MAIS.</article>
		    			</div>
		    	</div>	    			
	    		<div class='lines' id='terceiro-lines'></div>    			
    		</aside>

    		<aside class='quarta-parte'>
    			<div class='left-image'>
    				<img src='IMAGES/pensamento-criativo.svg' alt='Pintora com ideias' class='svg-image'>
    			</div>
    			<div class='container-news-paper'>
	    			<h3 class='title'>Para você !</h3>
		    			<div class='news-paper'><article>Possibilitamos que você crie, inove<br/> e atualize seu próprio site.</article>
		    			</div>
		    	</div>	    			    			
    		</aside>

    		<aside class='meet-our-work'>
    			<div class='left-image'>
    				<img src='IMAGES/olhandoMonitor.svg' alt='Pintora com ideias' class='svg-image' id='olhandoMonitor'>
    			</div>
    			<div class='container-news-paper'>
	    			<h3 class='conheca-title'>Conheça nosso trabalho !</h3>
		    			<div class='news-paper' id='news-paper'>
		    				<figure class='images'>	    			
	    						<div class='images-organization'>
	    							<div class='link-business'>
		    							<a href='CRUD/Visual/portfolio.php' id='link-port'>Portfólio</a> 
    								</div>	 
    							</div>   			
    						</figure>
	    			</div>
		    	</div>	
    		</aside>
    		<footer>
    			<div class='navegation'>
    				<aside class='organization-navegation'>
						<a href='CRUD/Visual/comoUsar.php' id='link' class='links'><article id='dicas-link'>Como usar ?</a>
						<a href='CRUD/Visual/ModificationSite.php' id='vms' class='links'>Vamos começar !</a>
						<a href='#here'><img src='IMAGES/backRow.png' alt='seta para subir ao topo de página' id='img-row'/></a>
					</aside>
				</div>
    		</footer>
		</section>    	
	</body>
</html>
	";
?>